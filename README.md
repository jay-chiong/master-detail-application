# MovieList
Show list of movies from https://itunes.apple.com/search?term=star&country=au&media=movie

# Persistence Mechanism
* Choose the sorting and ordering type (Track Name, Artist and Genre) (Ascending and Descending)
* Save the sorting and orderting type
* Use the saved sorting and ordering type when the app is reopened

`I chose this mechanism because it is the most useful functionality that all users are looking for when it comes to lists.`

# Architecture
* MVP pattern