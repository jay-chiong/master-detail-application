//
//  PlayListCell.swift
//  Playlist
//
//  Created by Jay on 24/12/2018.
//  Copyright © 2018 jay. All rights reserved.
//

import UIKit

class PlayListCell: UITableViewCell {
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var trackImageView: UIImageView!
    @IBOutlet weak var trackName: UILabel!
    @IBOutlet weak var artistName: UILabel!
    @IBOutlet weak var genre: UILabel!
    @IBOutlet weak var price: UILabel!
}
