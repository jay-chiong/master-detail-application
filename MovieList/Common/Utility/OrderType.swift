//
//  OrderType.swift
//  MovieList
//
//  Created by Jay on 26/12/2018.
//  Copyright © 2018 jay. All rights reserved.
//

public  enum OrderType: String {
    case ascending = "ascending"
    case descending = "descending"
}

