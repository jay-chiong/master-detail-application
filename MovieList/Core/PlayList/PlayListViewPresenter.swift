//
//  PlayListViewController.swift
//  Playlist
//
//  Created by Jay on 24/12/2018.
//  Copyright © 2018 jay. All rights reserved.
//

import UIKit

protocol PlayListPresenter: class {
    func getNumberOfResults() -> Int
    func fetchMedias()
    func getMediaData(index: Int) -> MediaResult?
    func setSelectedIndex(index: Int)
    func getSelectedData() -> MediaResult?
    func sortByTrackName(orderType: String)
    func sortByArtistName(orderType: String)
    func sortByGenre(orderType: String)
    func storeSortingData(orderType: String, sortType: Int)
    func getStoredOrderType() -> String?
    func getStoredSortType() -> Int?
}

final class PlayListViewPresenter: PlayListPresenter {
    
    private let cache = NSCache<NSString, NSObject>()
    
    private let client = ApiClient()
    private var response: MediaResultResponse?
    private var sortedResponse: MediaResultResponse?
    private var selectedIndex = 0
    
    private weak var view: PlayListView?
    
    init(view: PlayListView) {
        self.view = view
    }
    
    /// Return size of media
    func getNumberOfResults() -> Int {
        if self.sortedResponse != nil {
            if let count = self.sortedResponse?.resultCount {
                return count
            }
        }
        
        return 0
    }
    
    /// Fetch json data
    func fetchMedias() {
        if Storage.fileExists("mediaData.json", in: .caches) {
            self.retrieveData()
        } else {
            let request = ApiRequest.SearchForMedia(term: "star", country: "au", media: "movie")
            self.client.send(request: request, completion: { result in
                switch result {
                case .success(let value):
                    self.response = value
                    self.storeData()
                    self.sortByTrackName(orderType: OrderType.ascending.rawValue)
                    self.view?.reloadList()
                case .failure(let error):
                    print("Error: \(error)")
                }
            })
        }
    }
    
    /// Return data of specific index
    /// index: Index of selected data
    func getMediaData(index: Int) -> MediaResult? {
        if let medias = self.sortedResponse?.results {
            return medias[index]
        }
        
        return nil
    }
    
    /// Update selected index
    /// index: Index of selected data
    func setSelectedIndex(index: Int) {
        self.selectedIndex = index
    }
    
    /// Return data of selected index
    func getSelectedData() -> MediaResult? {
        if let media = getMediaData(index: self.selectedIndex) {
            return media
        }
        
        return nil
    }
    
    /// Sort media by track name
    /// orderType: Ascending or Descending
    func sortByTrackName(orderType: String) {
        var respo = self.response
        
        if orderType == OrderType.ascending.rawValue {
            if let sortedArray = self.response?.results.sorted(by: { $0.trackName < $1.trackName }) {
                respo?.results = sortedArray
                self.sortedResponse = respo
            }
        } else {
            if let sortedArray = self.response?.results.sorted(by: { $0.trackName > $1.trackName }) {
                respo?.results = sortedArray
                self.sortedResponse = respo
            }
        }
        
        self.view?.reloadList()
    }
    
    /// Sort media by artist name
    /// orderType: Ascending or Descending
    func sortByArtistName(orderType: String) {
        var respo = self.response
        
        if orderType == OrderType.ascending.rawValue {
            if let sortedArray = self.response?.results.sorted(by: { $0.artistName < $1.artistName }) {
                respo?.results = sortedArray
                self.sortedResponse = respo
            }
        } else {
            if let sortedArray = self.response?.results.sorted(by: { $0.artistName > $1.artistName }) {
                respo?.results = sortedArray
                self.sortedResponse = respo
            }
        }
        
        self.view?.reloadList()
    }
    
    /// Sort media by genre
    /// orderType: Ascending or Descending
    func sortByGenre(orderType: String) {
        var respo = self.response
        
        if orderType == OrderType.ascending.rawValue {
            if let sortedArray = self.response?.results.sorted(by: { $0.primaryGenreName < $1.primaryGenreName }) {
                respo?.results = sortedArray
                self.sortedResponse = respo
            }
        } else {
            if let sortedArray = self.response?.results.sorted(by: { $0.primaryGenreName > $1.primaryGenreName }) {
                respo?.results = sortedArray
                self.sortedResponse = respo
            }
        }
        
        self.view?.reloadList()
    }
    
    /// Store sortType and orderType to UserDefaults
    /// orderType: Ascending or Descending
    /// sortType: trackName = 1, artist = 2, genre = 3
    func storeSortingData(orderType: String, sortType: Int) {
        UserDefaults.standard.set(sortType, forKey: "sortType")
        UserDefaults.standard.set(orderType, forKey: "orderType")
    }
    
    /// Getter - Order Type
    func getStoredOrderType() -> String? {
        return UserDefaults.standard.string(forKey: "orderType")
    }
    
    /// Getter - Sort Type
    func getStoredSortType() -> Int? {
        return UserDefaults.standard.integer(forKey: "sortType")
    }
    
    /// Store json data to cache
    private func storeData() {
        Storage.store(self.response, to: .caches, as: "mediaData.json")
    }
    
    /// Retrieve json data
    private func retrieveData() {
        self.response = Storage.retrieve("mediaData.json", from: .caches, as: MediaResultResponse.self)
        self.sortByTrackName(orderType: OrderType.ascending.rawValue)
    }
}
