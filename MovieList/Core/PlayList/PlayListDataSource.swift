//
//  PlayListDataSource.swift
//  Playlist
//
//  Created by Jay on 24/12/2018.
//  Copyright © 2018 jay. All rights reserved.
//

import UIKit

class PlayListDataSource: NSObject {
    
    fileprivate let presenter: PlayListPresenter
    
    init(presenter: PlayListPresenter) {
        self.presenter = presenter
    }
    
    func configureTableView(tableView: UITableView) {
        tableView.delegate = self
        tableView.dataSource = self
    }
}

extension PlayListDataSource: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presenter.getNumberOfResults()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "playListCell") as! PlayListCell
        
        /// Draw shadow all over the cell
        cell.cellView.layer.masksToBounds = false
        cell.cellView.layer.shadowColor = UIColor.gray.cgColor
        cell.cellView.layer.shadowOffset = CGSize(width: 1, height: 1)
        cell.cellView.layer.shadowOpacity = 0.6
        cell.cellView.layer.shadowRadius = 2
        
        /// Check if there is media for an index
        if let media = self.presenter.getMediaData(index: indexPath.item) {
            cell.trackName.text = media.trackName
            cell.artistName.text = media.artistName
            cell.genre.text = media.primaryGenreName
            
            if let price = media.trackPrice {
                cell.price.text = "$ \(price)"
            }
            
            cell.trackImageView.image = nil
            
            if let trackImageUrl160 = media.artworkUrl160 {
                cell.trackImageView.loadImageUsingCacheWithUrlString(trackImageUrl160)
            } else if let trackImageUrl130 = media.artworkUrl130 {
                cell.trackImageView.loadImageUsingCacheWithUrlString(trackImageUrl130)
            } else if let trackImageUrl100 = media.artworkUrl100 {
                cell.trackImageView.loadImageUsingCacheWithUrlString(trackImageUrl100)
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        /// Update selected index in presenter
        self.presenter.setSelectedIndex(index: indexPath.item)
        
        /// Remove highlight after selecting
        tableView.deselectRow(at: indexPath, animated: false)
    }
}
