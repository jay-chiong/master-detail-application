//
//  ViewController.swift
//  Playlist
//
//  Created by Jay on 24/12/2018.
//  Copyright © 2018 jay. All rights reserved.
//

import UIKit

protocol PlayListView: class {
    func reloadList()
}

class PlayListViewController: UIViewController {
    
    private lazy var presenter: PlayListPresenter = PlayListViewPresenter(view: self)
    private lazy var dataSource: PlayListDataSource = .init(presenter: self.presenter)
    
    @IBOutlet weak var playListTableView: UITableView!
    @IBOutlet weak var sortViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var loadingView: UIView!
    
    // sorting buttons
    @IBOutlet weak var trackNameButton: UIButton!
    @IBOutlet weak var artistNameButton: UIButton!
    @IBOutlet weak var genreButton: UIButton!
    
    // default: trackName
    private var selectedSortTypeTag = 1
    // default: ascending
    private var orderType = OrderType.ascending.rawValue
    
    private var isSortViewVisible = false
    private var isFirstRun = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.dataSource.configureTableView(tableView: self.playListTableView)
        self.presenter.fetchMedias()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.hideButtonImage()
        
        /// Check from the UserDefaults the sortType and orderType
        if let storedSortType = self.presenter.getStoredSortType(), let storedOrderType = self.presenter.getStoredOrderType() {
            self.selectedSortTypeTag = storedSortType
            self.orderType = storedOrderType
            self.changeButtonImage(button: self.findButton())
            self.triggerSort(tag: self.findButton().tag)
        } else {
            self.trackNameButton.imageView?.layer.transform = CATransform3DIdentity
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is PlayListDetailViewController {
            let vc = segue.destination as? PlayListDetailViewController
            vc?.mediaData = self.presenter.getSelectedData()
        }
        
        if isSortViewVisible {
            self.sortTapped(sender: self)
        }
    }
    
    // MARK: ACTION - Sort Tap
    @IBAction func sortTapped(sender: AnyObject) {
        var height = 0
        
        if isSortViewVisible {
            height = -200
        }
        
        isSortViewVisible = !isSortViewVisible
        
        DispatchQueue.main.async {
            self.sortViewBottomConstraint.constant = CGFloat(height)
            self.startAnimation()
        }
    }
    
    // MARK: ACTION - Sort Option Tap
    @IBAction func sortOptionsTap(_ sender: AnyObject) {
        guard let button = sender as? UIButton else { return }
        
        if self.selectedSortTypeTag == button.tag {
            if orderType == OrderType.ascending.rawValue {
                orderType = OrderType.descending.rawValue
            } else {
                orderType = OrderType.ascending.rawValue
            }
        } else {
            orderType = OrderType.ascending.rawValue
        }
        
        self.changeButtonImage(button: button)
        self.selectedSortTypeTag = button.tag
        self.sortTapped(sender: self)
        self.triggerSort(tag: button.tag)
        
        self.presenter.storeSortingData(orderType: self.orderType, sortType: self.selectedSortTypeTag)
    }
    
    /// Animation when sort view is displayed and hide
    private func startAnimation() {
        UIView.animate(withDuration: 0.4, delay: 0.0, options: [], animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    /// Hide the image of button
    private func hideButtonImage() {
        self.trackNameButton.imageView?.layer.transform = CATransform3DMakeScale(0.0, 0.0, 0.0)
        self.artistNameButton.imageView?.layer.transform = CATransform3DMakeScale(0.0, 0.0, 0.0)
        self.genreButton.imageView?.layer.transform = CATransform3DMakeScale(0.0, 0.0, 0.0)
    }
    
    /// Change the button image from up/down depending on the button
    private func changeButtonImage(button: UIButton) {
        self.hideButtonImage()
        
        var image = UIImage(named: "arrow_up_icon")
        
        if self.orderType == OrderType.ascending.rawValue {
            image = UIImage(named: "arrow_up_icon")
        } else {
            image = UIImage(named: "arrow_down_icon")
        }
        
        button.setImage(image, for: .normal)
        button.imageView?.layer.transform = CATransform3DIdentity
    }
    
    /// Call the sorting process
    private func triggerSort(tag: Int) {
        switch tag {
        case 1: // trackName
            self.presenter.sortByTrackName(orderType: self.orderType)
        case 2: // artistName
            self.presenter.sortByArtistName(orderType: self.orderType)
        case 3: // genre
            self.presenter.sortByGenre(orderType: self.orderType)
        default:
            break
        }
    }
    
    /// Get button based on the sort type
    private func findButton() -> UIButton {
        switch self.selectedSortTypeTag {
        case 1:
            return self.trackNameButton
        case 2:
            return self.artistNameButton
        case 3:
            return self.genreButton
        default:
            break
        }
        
        return self.trackNameButton
    }
}

// MARK: Extension
extension PlayListViewController: PlayListView {
    /// Reload tableView
    func reloadList() {
        DispatchQueue.main.async {
            self.playListTableView.reloadData()
        }
    }
}
