//
//  PlayListDetailViewController.swift
//  Playlist
//
//  Created by Jay on 24/12/2018.
//  Copyright © 2018 jay. All rights reserved.
//

import UIKit

class PlayListDetailViewController: UIViewController {
    
    @IBOutlet weak var trackImage: UIImageView!
    @IBOutlet weak var trackName: UILabel!
    @IBOutlet weak var artistName: UILabel!
    @IBOutlet weak var genre: UILabel!
    @IBOutlet weak var longDescription: UILabel!
    
    var mediaData: MediaResult?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.populateViews()
    }
    
    /// Populate views based from the data passed
    private func populateViews() {
        if let media = self.mediaData {
            if let trackImageUrl160 = media.artworkUrl160 {
                self.trackImage.loadImageUsingCacheWithUrlString(trackImageUrl160)
            } else if let trackImageUrl130 = media.artworkUrl130 {
                self.trackImage.loadImageUsingCacheWithUrlString(trackImageUrl130)
            } else if let trackImageUrl100 = media.artworkUrl100 {
                self.trackImage.loadImageUsingCacheWithUrlString(trackImageUrl100)
            }
            
            self.trackName.text = media.trackName
            self.artistName.text = media.artistName
            self.genre.text = media.primaryGenreName
            
            if let longDescription = media.longDescription {
                self.longDescription.text = longDescription
            }
        }
    }
}
