//
//  ApiRequest.swift
//  Playlist
//
//  Created by Jay on 24/12/2018.
//  Copyright © 2018 jay. All rights reserved.
//

import Foundation

final public class ApiRequest {
    
    public struct SearchForMedia: ApiRequestBuilder {
        
        private let term: String
        private let country: String
        private let media: String
        
        init(term: String, country: String, media: String) {
            self.term = term
            self.country = country
            self.media = media
        }
        
        public var parameters: Any? {
            return ["term": term, "country": country, "media": media]
        }
        
        public var path: String {
            return "/search"
        }
        
        public var method: HTTPMethod {
            return .get
        }
        
        public typealias Response = MediaResultResponse
        
        
    }
}
