//
//  ApiError.swift
//  Playlist
//
//  Created by Jay on 24/12/2018.
//  Copyright © 2018 jay. All rights reserved.
//

struct ApiError: Codable, Error {
    public let error: String?
    public let message: String?
}
