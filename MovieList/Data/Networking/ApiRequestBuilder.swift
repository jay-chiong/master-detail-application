//
//  ApiRequestBuilder.swift
//  Playlist
//
//  Created by Jay on 24/12/2018.
//  Copyright © 2018 jay. All rights reserved.
//

import Foundation

public protocol ApiRequestBuilder {
    associatedtype Response: Codable
    
    var baseURL: URL { get }
    var path: String { get }
    var parameters: Any? { get }
    var method: HTTPMethod { get }
}

extension ApiRequestBuilder {
    public var baseURL: URL {
        return URL(string: "https://itunes.apple.com")!
    }
    
    func buildURLRequest() -> URLRequest {
        let url = baseURL.appendingPathComponent(path)
        var components = URLComponents(url: url, resolvingAgainstBaseURL: true)
        var urlRequest = URLRequest(url: url)
        
        switch method {
        case .get:
            // HTTP Method GET function
            let dictionary = parameters as? [String: Any]
            let queryItem = dictionary?.map { (arg) -> URLQueryItem  in
                let (key, value) = arg
                return URLQueryItem(name: key, value: value as? String)
            }
            
            components?.queryItems = queryItem
            urlRequest.url = components?.url
        default:
            // default function
            fatalError("Unsupported method \(method)")
        }
        
        urlRequest.httpMethod = method.rawValue
        
        return urlRequest
    }
    
    func response(from data: Data, urlResponse: URLResponse) throws -> Response {
        print("\(#function): \(data)")
        if case (200..<300)? = (urlResponse as? HTTPURLResponse)?.statusCode {
            return try JSONDecoder().decode(Response.self, from: data)
        } else {
            throw try JSONDecoder().decode(ApiError.self, from: data)
        }
    }
    
}
