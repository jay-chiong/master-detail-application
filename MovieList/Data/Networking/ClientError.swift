//
//  ClientError.swift
//  Playlist
//
//  Created by Jay on 24/12/2018.
//  Copyright © 2018 jay. All rights reserved.
//

public enum ClientError: Error {
    case connectionError(Error)
    case responseParseError(Error)
    case apiError(Error)
}
