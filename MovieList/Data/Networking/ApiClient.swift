//
//  ApiClient.swift
//  Playlist
//
//  Created by Jay on 24/12/2018.
//  Copyright © 2018 jay. All rights reserved.
//

import Foundation

public class ApiClient {
    private let session: URLSession = {
        let configration = URLSessionConfiguration.default
        let session = URLSession(configuration: configration)
        return session
    }()
    
    public init() { }

    public func send<Request: ApiRequestBuilder> (request: Request, completion: @escaping (Result<Request.Response, ClientError>) -> Void) {
        let urlRequest = request.buildURLRequest()
        let task = session.dataTask(with: urlRequest) { (data, response, error) in
            
            switch (data, response, error) {
            case (_, _, let error?) :
                completion(Result(error: .connectionError(error)))
            case (let data?, let response?, _):
                do {
                    let response = try request.response(from: data, urlResponse: response)
                    completion(Result(value: response))
                } catch let error as ClientError {
                    completion(Result(error: .apiError(error)))
                } catch {
                    completion(Result(error: .responseParseError(error)))
                }
            default:
                fatalError("invalid response combination \(String(describing: data) ), \(String(describing: response)), \(String(describing: error)).")
            }
        }
        task.resume()
    }
}
