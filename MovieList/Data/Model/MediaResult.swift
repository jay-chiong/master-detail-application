//
//  MediaResult.swift
//  Playlist
//
//  Created by Jay on 24/12/2018.
//  Copyright © 2018 jay. All rights reserved.
//

public struct MediaResult: Codable {
    public let wrapperType: String?
    public let kind: String?
    public let collectionId: Int?
    public let trackId: Int?
    public let artistName: String
    public let collectionName: String?
    public let trackName: String
    public let collectionCensoredName: String?
    public let trackCensoredName: String?
    public let collectionArtistId: Int?
    public let collectionArtistViewUrl: String?
    public let collectionViewUrl: String?
    public let trackViewUrl: String?
    public let previewUrl: String?
    public let artworkUrl130: String?
    public let artworkUrl160: String?
    public let artworkUrl100: String?
    public let collectionPrice: Double?
    public let trackPrice: Double?
    public let collectionHdPrice: Double?
    public let releaseDate: String?
    public let collectionExplicitness: String?
    public let trackExplicitness: String?
    public let discCount: Int?
    public let discNumber: Int?
    public let trackCount: Int?
    public let trackNumber: Int?
    public let trackTimeMillis: Int?
    public let country: String?
    public let currency: String?
    public let primaryGenreName: String
    public let contentAdvisoryRating: String?
    public let shortDescription: String?
    public let longDescription: String?
    public let hasITunesExtra: Bool?
}
