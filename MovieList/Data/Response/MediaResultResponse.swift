//
//  MediaResultResponse.swift
//  Playlist
//
//  Created by Jay on 24/12/2018.
//  Copyright © 2018 jay. All rights reserved.
//

import Foundation

public struct MediaResultResponse: Codable {
    public let resultCount: Int
    public var results: [MediaResult]
}
